﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class adjustDataTranslation : MonoBehaviour
{

    public Text debug;

    // public GameObject[] attButtons;

    public Transform data;

    public int direction;   //1,2 - forward/backward; 3/4 - right/left; 5/6 - up/down

    private float transIncrease;

    public bool freeTranslating;



    // Use this for initialization
    void Start()
    {
        transIncrease = 0.3f;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnSelect()
    {
        //attButtons = GameObject.FindGameObjectsWithTag("attButton");

        //string attribute;
        // attribute = gameObject.name;

        if (freeTranslating)
        {
            data.GetComponent<postParsingDataController>().translationSelected(direction);
        }

        else
        {
            switch (direction)
            {
                case 1:
                    data.position = new Vector3(data.position.x, data.position.y, data.position.z - transIncrease);
                    

                    break;
                case 2:
                    data.position = new Vector3(data.position.x, data.position.y, data.position.z + transIncrease);
                    
                    break;
                case 3:
                    data.position = new Vector3(data.position.x - transIncrease, data.position.y, data.position.z);
                    
                    break;
                case 4:
                    data.position = new Vector3(data.position.x + transIncrease, data.position.y, data.position.z);
                    
                    break;
                case 5:
                    data.position = new Vector3(data.position.x, data.position.y + transIncrease, data.position.z);
                    
                    break;
                case 6:
                    data.position = new Vector3(data.position.x, data.position.y - transIncrease, data.position.z);

                    break;

            }

            StartCoroutine(CustomCoroutineAsync());

            gameObject.GetComponent<MeshRenderer>().enabled = false;
        }






        


    }

    IEnumerator CustomCoroutineAsync()
    {
        yield return new WaitForSeconds(0.1f);

        gameObject.GetComponent<MeshRenderer>().enabled = true;
    }



}

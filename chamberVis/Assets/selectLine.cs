﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class selectLine : MonoBehaviour {

    public GameObject canvasDB;
    public Text dbTextValue;

    public Transform cursor;
    public Transform fieldCenter;

    bool selected;

    // Use this for initialization
    void Start () {
        selected = false;

    }
	
	// Update is called once per frame
	void Update () {

        if(selected)
        {
            canvasDB.SetActive(true);

            float intensity = float.Parse(gameObject.GetComponent<Text>().text);

            float dist = Vector3.Distance(cursor.position, fieldCenter.position);

            if (dist > 1)
                intensity = intensity / dist;

            dbTextValue.text = intensity.ToString("0.###") + " dB";
        }
		
	}

    void OnSelect()
    {
        selected = !selected;

        //canvasDB.SetActive(selected);

        if (selected)
        {
            foreach (Transform child in transform.parent)
            {
                child.gameObject.GetComponent<BoxCollider>().enabled = false;
            }

            gameObject.GetComponent<BoxCollider>().enabled = true;

        }
        else
        {
            foreach (Transform child in transform.parent)
            {
                child.gameObject.GetComponent<BoxCollider>().enabled = true;
            }

            dbTextValue.text = "Last value:\n" + dbTextValue.text;

        }
        

    }


}

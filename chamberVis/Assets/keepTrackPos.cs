﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class keepTrackPos : MonoBehaviour {

    public Text localPosText, worldPosText, rotText;
    public Text origLocalPosText, origWorldPosText, origRotText;

    // Use this for initialization
    void Start () {

        origLocalPosText.text = "Local Pos: X: " + transform.localPosition.x + " Y: " + transform.localPosition.y + " Z: " + transform.localPosition.z;
        origWorldPosText.text = "World Pos: X: " + transform.position.x + " Y: " + transform.position.y + " Z: " + transform.position.z;
        origRotText.text = "Rot: X: " + transform.localEulerAngles.x + " Y: " + transform.localEulerAngles.y + " Z: " + transform.localEulerAngles.z;


    }

    // Update is called once per frame
    void Update () {
        localPosText.text = "Local Pos: X: " + transform.localPosition.x + " Y: " + transform.localPosition.y + " Z: " + transform.localPosition.z;
        worldPosText.text = "World Pos: X: " + transform.position.x + " Y: " + transform.position.y + " Z: " + transform.position.z;
        rotText.text = "Rot: X: " + transform.localEulerAngles.x + " Y: " + transform.localEulerAngles.y + " Z: " + transform.localEulerAngles.z;

    }


}

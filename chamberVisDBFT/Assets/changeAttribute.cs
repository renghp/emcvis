﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class changeAttribute : MonoBehaviour
{
    //public GameObject seatScoreController;

    public Material defaultMat, highlightedMat;

    public Text debug;

    public GameObject[] attButtons;

    public bool holdable;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnSelect()
    {
        //attButtons = GameObject.FindGameObjectsWithTag("attButton");

       // string attribute;
        // = gameObject.name;

        if (!holdable)
        {
            foreach (GameObject aB in attButtons)
                aB.GetComponent<MeshRenderer>().enabled = true;


            gameObject.GetComponent<MeshRenderer>().enabled = false;
        }
        

       // seatScoreController.GetComponent<seatScores67>().changeAttribute(transform.GetSiblingIndex());

    }

    public void selectedOff()
    {
        gameObject.GetComponent<MeshRenderer>().enabled = true;
    }

    public void cursorHit()
    {
        Debug.Log("colidiu");
        gameObject.GetComponent<Renderer>().material = highlightedMat;
    }

    public void unHit()
    {
        Debug.Log("descolidiu");
        gameObject.GetComponent<Renderer>().material = defaultMat;
    }


}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class handAjustAlongLine : MonoBehaviour {

    public Transform currentHandPosition;
    private Vector3 initialHandPosition;

    public Text debugText;
    private float initialZ;

	// Use this for initialization
	void Start () {

        initialHandPosition = currentHandPosition.localPosition;
        initialZ = transform.localPosition.z;

    }

    void OnEnable()
    {
        initialHandPosition = currentHandPosition.localPosition;
        initialZ = transform.localPosition.z;
    }

    // Update is called once per frame
    void Update () {

        //float newZ = Vector3.Distance(initialHandPosition, currentHandPosition.localPosition);// * 0.1f;

        float newZ = (initialHandPosition.x - currentHandPosition.localPosition.x) * 0.5f;


        //errorTextField1.text = "dist: "+dist.ToString();

       /* if (currentHandPosition.localPosition.x > initialHandPosition.x)
        {
            debugText.text = "og Z:"+ initialZ + " newZ:" + newZ;*/

            newZ = initialZ + newZ;

            
       /* }
        else
        {
            debugText.text = "og Z:" + initialZ + " newZ:" + newZ;

            newZ = initialZ + newZ;

           
        }*/

        if ((newZ > 0f) && (newZ < 1f))
            transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, newZ);
        else if (newZ < 1f)
            transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, 0f);
        else
            transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, 1f);
        /////






    }
}

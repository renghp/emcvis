﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class changeFreq : MonoBehaviour {

    public GameObject fieldRenderer;
    public Transform placeHolder;
    public string parsedCartesianFilesPath;

    public Text errorTextField1;

    public bool increase;

    ArrayList filePaths = new ArrayList();

    public int currentFile;

    public GameObject siblingButton;

    public int fieldNumber;

    // Use this for initialization
    void Start () {

       // errorTextField1.text = "starting reading files";

        //DirectoryInfo dir = new DirectoryInfo(parsedCartesianFilesPath);


        var loadedObjects = Resources.LoadAll("tempParsingFiles");      //UNLOAD THESE AFTER THE FOREACH!!

       // Debug.Log("var is " + loadedObjects[0].name);

        //errorTextField1.text = "read directory";

        //FileInfo[] info = dir.GetFiles("*.txt");                //DOES NOT WORK ON HL

        //errorTextField1.text = "got txt files";

        foreach (var lo in loadedObjects)
        {
            //errorTextField1.text = "reading first file name: " + lo.name;

            string a = lo.name;
            //errorTextField1.text = "tempParsingFiles\\" + a;

            filePaths.Add("tempParsingFiles\\" + a);

            Resources.UnloadAsset(lo);
        }


        if (currentFile < 0)
        {
            currentFile = filePaths.Count - 1;
        }

        string newFilePath = filePaths[currentFile % filePaths.Count].ToString();

        string freqStr = newFilePath.Substring(17);

        float freqFloat = float.Parse(freqStr);     // in Hz

        freqFloat = freqFloat / 1000000000;     //in GHz


        errorTextField1.text = "Field #" + fieldNumber + "\nFrequency:\n" + freqFloat.ToString("0.###") + " GHz";



        //errorTextField1.text = "# of files: " + filePaths.Count;

        //Debug.Log(filePaths[0]);

    }
	
	// Update is called once per frame
	void Update () {
        transform.position = placeHolder.position;
        transform.eulerAngles = placeHolder.eulerAngles;
    }

    void OnSelect()
    {
       // errorTextField1.text = "button pressed";

        if (increase)
        {
            currentFile++;

           // errorTextField1.text = "next file is " + currentFile;
        }

        else
        {
            currentFile--;

            if (currentFile < 0)
            {
                currentFile = filePaths.Count - 1;
            }
           // errorTextField1.text = "next file is " + currentFile;
        }

        siblingButton.GetComponent<changeFreq>().currentFile = currentFile;

        //errorTextField1.text = "next will be of index " + (currentFile % filePaths.Count).ToString();

        string newFilePath = filePaths[currentFile%filePaths.Count].ToString();

        string freqStr = newFilePath.Substring(17);

        float freqFloat = float.Parse(freqStr);     // in Hz

        freqFloat = freqFloat / 1000000000;     //in GHz

        errorTextField1.text = "Field #" + fieldNumber + "\nFrequency:\n" + freqFloat.ToString("0.###") + " GHz";




        fieldRenderer.GetComponent<simplifiedDataLoader>().changeFreq(newFilePath);

    }
}

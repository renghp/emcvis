﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class resetDataTransform : MonoBehaviour
{


    public GameObject dataParent;

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnSelect()
    {

            dataParent.GetComponent<postParsingDataController>().resetTransform();


            StartCoroutine(CustomCoroutineAsync());

            gameObject.GetComponent<MeshRenderer>().enabled = false;

    }

    IEnumerator CustomCoroutineAsync()
    {
        yield return new WaitForSeconds(0.1f);

        gameObject.GetComponent<MeshRenderer>().enabled = true;
    }



}


﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

#if !UNITY_EDITOR
using System.Threading.Tasks;
#endif

public class clientDualSystem : MonoBehaviour
{
    public Text feedback;
    public Text inputHost;
    public bool getNewFile = true;
    public bool twoDFileReady = false;

    public GameObject twoDField;

    bool alreadyConnected = false;


    //public USTrackingManager TrackingManager;
    //public USStatusTextManager StatusTextManager;

#if !UNITY_EDITOR
    private bool _useUWP = true;
    private Windows.Networking.Sockets.StreamSocket socket;
    private Task exchangeTask;
#endif

#if UNITY_EDITOR
    private bool _useUWP = false;
    System.Net.Sockets.TcpClient client;
    System.Net.Sockets.NetworkStream stream;
    private Thread exchangeThread;
#endif

    private Byte[] bytes = new Byte[256];
    private StreamWriter writer;
    private StreamReader reader;

    public void Start()
    {

#if UNITY_EDITOR
        OnSelect();
#endif
    }


    public void OnSelect()
    {

        getNewFile = true;

        if (!alreadyConnected)
        { 

            string host = "192.168.137.1";
            int port = 6321;

            if (inputHost.text != "" && inputHost.text != null)
                host = inputHost.text;

            feedback.text = "Connecting to " + host + " on port " + port.ToString();


            Connect(host, port.ToString());
        }
    }

    public void Connect(string host, string port)
    {
        if (_useUWP)
        {
            ConnectUWP(host, port);
        }
        else
        {
            ConnectUnity(host, port);
        }
    }



#if UNITY_EDITOR
    private void ConnectUWP(string host, string port)
#else
    private async void ConnectUWP(string host, string port)
#endif
    {
#if UNITY_EDITOR
        feedback.text = "UWP TCP client used in Unity!";
#else
        try
        {
            if (exchangeTask != null) StopExchange();
        
            socket = new Windows.Networking.Sockets.StreamSocket();
            Windows.Networking.HostName serverHost = new Windows.Networking.HostName(host);
            await socket.ConnectAsync(serverHost, port);
        
            Stream streamOut = socket.OutputStream.AsStreamForWrite();
            writer = new StreamWriter(streamOut) { AutoFlush = true };
        
            Stream streamIn = socket.InputStream.AsStreamForRead();
            reader = new StreamReader(streamIn);

            //feedback.text = "Connected!";

            RestartExchange();
        }
        catch (Exception e)
        {
            feedback.text = "Error " + e.ToString();
        }
#endif
    }

    private void ConnectUnity(string host, string port)
    {
#if !UNITY_EDITOR
        errorStatus = "Unity TCP client used in UWP!";
#else
        try
        {
            if (exchangeThread != null) StopExchange();

            client = new System.Net.Sockets.TcpClient(host, Int32.Parse(port));
            stream = client.GetStream();
            reader = new StreamReader(stream);
            writer = new StreamWriter(stream) { AutoFlush = true };

            feedback.text = "Connected!";

            alreadyConnected = true;

            RestartExchange();
            
        }
        catch (Exception e)
        {
            feedback.text = "Error " + e.ToString();
        }
#endif
    }

    private bool exchanging = false;
    private bool exchangeStopRequested = false;
    private string lastPacket = null;

    private string errorStatus = null;
    private string warningStatus = null;
    private string successStatus = null;
    private string unknownStatus = null;

    public void RestartExchange()
    {
#if UNITY_EDITOR
        if (exchangeThread != null)
            StopExchange();

        exchangeStopRequested = false;
        exchangeThread = new System.Threading.Thread(ExchangePackets);
        exchangeThread.Start();
#else
        if (exchangeTask != null) 
            StopExchange();

        exchangeStopRequested = false;





        exchangeTask = Task.Run(() => ExchangePackets());
#endif
    }

    public void Update()
    {



        if (lastPacket != null)
        {

            if (lastPacket.Length < 30)
            {
                feedback.text = "Problems with the file from the server. Loading offline version.";
            }

            else
            { 
                feedback.text = "File \"" + lastPacket.Substring(1, 20) + "(...)\" loaded";       //só escrever aqui no update

                if (twoDFileReady)
                {
                    twoDField.GetComponent<parse2DcolorCodedPlane>().fileFromServer = lastPacket;
                    twoDField.GetComponent<parse2DcolorCodedPlane>().twoDFileParserFromServer();

                    twoDFileReady = false;
                }
            }

            
        }

      /*  if (errorStatus != null)
        {
            StatusTextManager.SetError(errorStatus);
            errorStatus = null;
        }
        if (warningStatus != null)
        {
            StatusTextManager.SetWarning(warningStatus);
            warningStatus = null;
        }
        if (successStatus != null)
        {
            StatusTextManager.SetSuccess(successStatus);
            successStatus = null;
        }
        if (unknownStatus != null)
        {
            StatusTextManager.SetUnknown(unknownStatus);
            unknownStatus = null;
        }*/
    }

    public void ExchangePackets()
    {
        Debug.Log("vai entrar no while");

        string received = null;
        string lastLine = null;

        while (!exchangeStopRequested)   
         {



            //Debug.Log("while");

            // Debug.Log("entrou no while");

            if (writer == null || reader == null) continue;
            exchanging = true;

            writer.Write("X\n");
             //Debug.Log( "Sent data!");
             
            String newLine = reader.ReadLine() + "\n";


            if (getNewFile)
            {


                if (newLine.Contains("¢"))
                {
                    received = null;

                }

                received += newLine;

                

                lastPacket = received;

                if (newLine.Contains("£"))
                {
                    getNewFile = false;
                    twoDFileReady = true;
                }

            }
            // Debug.Log("Received data: " + received);

             exchanging = false;
         }

   
    }





public void StopExchange()
{
 exchangeStopRequested = true;
 alreadyConnected = false;

#if UNITY_EDITOR
 if (exchangeThread != null)
 {
     exchangeThread.Abort();
     stream.Close();
     client.Close();
     writer.Close();
     reader.Close();

     stream = null;
     exchangeThread = null;
 }
#else
 if (exchangeTask != null) {
     exchangeTask.Wait();
     socket.Dispose();
     writer.Dispose();
     reader.Dispose();

     socket = null;
     exchangeTask = null;
 }
#endif
 writer = null;
 reader = null;
}

public void OnDestroy()
{
 StopExchange();
}

}

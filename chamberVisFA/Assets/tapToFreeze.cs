﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloLensHandTracking;

public class tapToFreeze : MonoBehaviour {

    bool frozen = false;

    // public GameObject TrackingObject;

    //public GameObject TrackingObject2;

    public Transform fatherObject;

    Transform originalParent;

    Transform originalTransform;

    public Material selectedMat, unselectedMat;

    

    // Use this for initialization
    void Start () {

        originalParent = fatherObject.parent;
        originalTransform = fatherObject;

    }
	
	// Update is called once per frame
	void Update () {

        /*if (handTracker.holdingHand1)
        {
            if (frozen)
            {
                float size = Vector3.Distance(TrackingObject.transform.localPosition, TrackingObject2.transform.localPosition);
                transform.localScale = new Vector3(size, size, size);
            }
        }*/ 


    }

    void OnSelect()
    {
        frozen = !frozen;

       

        if (frozen)
        {
            fatherObject.parent = transform;
            fatherObject.parent = Camera.main.transform;

            if (gameObject.tag == "3Ddata")
            {
                gameObject.GetComponent<LineRenderer>().material = selectedMat;
            }
            

        }
        else
        {
            
            fatherObject.parent = originalParent;
            fatherObject.localEulerAngles = new Vector3(0f, fatherObject.localEulerAngles.y, 0f);     //preserves the pitch and roll rotations (hololens takes care of them), only changing the yaw

            if (gameObject.tag == "3Ddata")
            {
                gameObject.GetComponent<LineRenderer>().material = unselectedMat;
            }
        }



    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class adjustDataRotation : MonoBehaviour
{

    public Text debug;

    // public GameObject[] attButtons;

    public Transform data;

    public bool clockwise;

    private float rotIncrease;

    public bool freeRotating;

    // Use this for initialization
    void Start()
    {
        rotIncrease = 10f;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnSelect()
    {


        if (freeRotating)
        {
            data.GetComponent<postParsingDataController>().rotSelected();
            // canvasScale.SetActive(true);
        }

        else if (clockwise)
        {
            //debug.text = "vai aumentar";
            data.localEulerAngles = new Vector3(data.localEulerAngles.x, data.localEulerAngles.y + rotIncrease, data.localEulerAngles.z);
            // debug.text = "x scale:\n" + data.localScale.x;

            StartCoroutine(CustomCoroutineAsync());

            gameObject.GetComponent<MeshRenderer>().enabled = false;

        }
        else
        {
            //debug.text = "vai diminuir";
            data.localEulerAngles = new Vector3(data.localEulerAngles.x, data.localEulerAngles.y - rotIncrease, data.localEulerAngles.z);
            // debug.text = "x scale:\n" + data.localScale.x;

            StartCoroutine(CustomCoroutineAsync());

            gameObject.GetComponent<MeshRenderer>().enabled = false;
        }


    }

    IEnumerator CustomCoroutineAsync()
    {
        yield return new WaitForSeconds(0.1f);

        gameObject.GetComponent<MeshRenderer>().enabled = true;
    }



}

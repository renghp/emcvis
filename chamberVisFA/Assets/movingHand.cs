﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movingHand : MonoBehaviour {

    bool rightLeft = true;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (rightLeft && transform.localPosition.x < 7f)
            transform.localPosition = new Vector3(transform.localPosition.x + 0.7f, transform.localPosition.y, transform.localPosition.z);
        else if (rightLeft)
            rightLeft = false;
        else if (transform.localPosition.x > -7f)
            transform.localPosition = new Vector3(transform.localPosition.x - 0.7f, transform.localPosition.y, transform.localPosition.z);
        else
            rightLeft = true;


    }
}

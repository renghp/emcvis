﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using System;
using System.IO;
using System.Text;
using UnityEngine.UI;




public class parse2DcolorCodedPlane : MonoBehaviour
{

    public string dataFileName;

    public bool dataIs2D;


    public Transform prefab;



    private Vector3 pointFrozen;



    public Material dataMat, scalingMat;

    private float baselineValue;
    private float toplineValue;



    // Use this for initialization
    void Start()
    {
        

        gameObject.GetComponent<LineRenderer>().material = dataMat;

        //gameObject.GetComponent<LineRenderer>().alignment = LineAlignment.TransformZ;




         twoDFileParser();





    }



    void twoDFileParser()
    {

        TextAsset dataAsset = Resources.Load(dataFileName) as TextAsset;     //filename without the frigging extension

        Debug.Log(dataAsset);


        string patternValues = @"(-?[0-9]*.[0-9]*e(\+|-)[0-9]*) ((-)?[0-9]*.[0-9]*e(\+|-)[0-9]*)"; //regex to get all values

        //

        Regex regexValues0 = new Regex(patternValues, RegexOptions.IgnoreCase);                                        //need to ignore the first (which is a duplicate of the final endlocation) and 
                                                                                                                       //get groups 2 and 4 as the latitude and longitude, respectively
        Match matchesValues0 = regexValues0.Match(dataAsset.text);


        baselineValue = (float)Double.Parse(matchesValues0.Groups[3].Value);    //first value
        toplineValue = (float)Double.Parse(matchesValues0.Groups[3].Value);    //first value

        while (matchesValues0.Success)
        {
            //Debug.Log("baseline defined as: " + baselineValue);
            //Debug.Log("valor 1: "+matchesValues.Groups[1].Value);
            //Debug.Log("valor 2: " + matchesValues.Groups[3].Value);

            float currentVal = (float)Double.Parse(matchesValues0.Groups[3].Value);

            if (currentVal < baselineValue)
            {
                baselineValue = currentVal;          //adjusting to the smallest one
            }

            if (currentVal > toplineValue)
            {
                toplineValue = currentVal;          //adjusting to the biggest one
            }


            matchesValues0 = matchesValues0.NextMatch();

        }

        Debug.Log("baseline defined as: " + baselineValue);

        baselineValue *= -1;                        //making it positive



        //


        Regex regexValues = new Regex(patternValues, RegexOptions.IgnoreCase);                                        
                                                                                                                     
        Match matchesValues = regexValues.Match(dataAsset.text);
        int matchCount = 1;


        while (matchesValues.Success)
        {

            if (matchCount%3 == 0)
            { 

                Transform a;

                a = Instantiate(prefab, SphericalToCartesian2D((float)Double.Parse(matchesValues.Groups[1].Value), (float)Double.Parse(matchesValues.Groups[3].Value)), Quaternion.identity, transform);
                a.localPosition = new Vector3(a.position.x, a.position.y, a.position.z);

                a.LookAt(transform);
                a.localEulerAngles = new Vector3(a.localEulerAngles.x, a.localEulerAngles.y + 180f, a.localEulerAngles.z);

                float percent = (1 / (toplineValue - ((baselineValue * -1))) * (float)Double.Parse(matchesValues.Groups[3].Value) ) - ((baselineValue*-1) / (toplineValue - (baselineValue * -1)));
                Debug.Log(percent);

                Color currentColor;

                if (percent > 0.5f)
                {
                    currentColor = new Color(percent, 1f, 1 - percent, 1f);
                }
                else //if (percent > 0.25f)
                {
                    currentColor = new Color(percent, percent * 2f, 1 - percent, 1f);
                }


                Color finalColor = new Color(currentColor.r, currentColor.g, currentColor.b, 0f);


                a.GetComponent<LineRenderer>().SetColors(currentColor, finalColor);


            }
            matchesValues = matchesValues.NextMatch();
            matchCount++;
        }

        float simplifyIndex = 0.1f;




        prefab.gameObject.SetActive(false);

        BoxCollider bc = gameObject.AddComponent<BoxCollider>();
    }

    // Update is called once per frame
    void Update()
    {

        



    }

    void OnSelect()
    {
        
    }



    public Vector3 SphericalToCartesian2D(float polar, float radius)
    {
        Vector3 outCart;

        radius = -(radius + baselineValue) / 20;              //runs through all the data and collect the lowest value as the "baseline", ie the most negative value in the data. I adjusted it like this so the origin is set on it. 
                                                             

       // Debug.Log(radius);

        float a = radius * Mathf.Cos(0);
        outCart.x = a * Mathf.Cos(polar);
        outCart.y = radius * Mathf.Sin(0);
        outCart.z = a * Mathf.Sin(polar);

        return outCart;
    }



}

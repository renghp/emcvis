﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using System;
using System.IO;
using System.Text;
using UnityEngine.UI;
using System.Threading;



public class parse3DFileWithoutSplit : MonoBehaviour
{
    public Transform prefab;

    public string dataFileName;


    public Text errorTextField1;
    public Text errorTextField2;

    private int nFiles;

    private Thread _t1;
    private Thread _t2;

    TextAsset dataAsset;

    string patternValues;

    Regex regexValues0;

    Match matchesValues0;

    public GameObject fileSimplifier;

    List<String> chosenFiles;

    bool readyForSimplifying1, readyForSimplifying2 = false;

    // Use this for initialization
    public void Start()
    {

        chosenFiles = new List<string>();


        /* dataAsset = Resources.Load(dataFileName) as TextAsset;

         Debug.Log("hej");

         Debug.Log(dataAsset.text);*/



        //Resources.UnloadAsset(dataAsset);


        _t1 = new Thread(_threeDFileParser);

        _t1.Start();
    }

    


    void _threeDFileParser()
    {

        readyForSimplifying1 = false;

        //Read the text from directly from the test.txt file
        StreamReader reader = new StreamReader(dataFileName);
        //Debug.Log(reader.ReadToEnd());

        string fileContent;

        fileContent = reader.ReadToEnd();

        reader.Close();

        //patternValues = @"(-?[0-9]*.[0-9]*)\t(-?[0-9]*.[0-9]*)\t(-?[0-9]*.[0-9]*)\t(-?[0-9]*.[0-9]*)\t(-?[0-9]*.[0-9]*)\t(-?[0-9]*.[0-9]*)"; //regex to get all values

        patternValues = @"([0-9]*)(.*)\n"; //regex to get all values

        
           //

           regexValues0 = new Regex(patternValues, RegexOptions.IgnoreCase);

        matchesValues0 = regexValues0.Match(fileContent);

        Debug.Log("parser");






        int countFiles = -1;
        //int matchCount = 1;


        if(matchesValues0.Success)                          //discard the first two lines, column descriptions
        {
            matchesValues0 = matchesValues0.NextMatch();
            matchesValues0 = matchesValues0.NextMatch();
        }

        while (matchesValues0.Success)
        {


            countFiles++;

           


            if (countFiles % 50 == 0)       //instead of the 200 files, gets only 5
            {

                //Debug.Log("pegou " + countFiles);

                chosenFiles.Add(matchesValues0.Groups[1].Value);

                string path = "Assets\\Resources\\tempParsingFiles\\" + matchesValues0.Groups[1].Value + ".txt";

                string currentFile = matchesValues0.Groups[1].Value;

                using (var tw = new StreamWriter(path, true))
                {


                    //tw.WriteLine(matchesValues0.Groups[1].Value + "\t" + matchesValues0.Groups[2].Value);
                    tw.WriteLine(matchesValues0.Groups[2].Value);
                    //tw.WriteLine(matchesValues0.Groups[1].Value + "\t" + matchesValues0.Groups[2].Value + "\t" + matchesValues0.Groups[3].Value + "\t" + matchesValues0.Groups[4].Value + "\t" + matchesValues0.Groups[5].Value + "\t" + matchesValues0.Groups[6].Value);
                    //tw.WriteLine(matchesValues0.Groups[2].Value + "\t" + matchesValues0.Groups[3].Value + "\t" + matchesValues0.Groups[6].Value);

                    matchesValues0 = matchesValues0.NextMatch();

                    //matchCount++;

                    while (currentFile == matchesValues0.Groups[1].Value)
                    {
                        if (matchesValues0.Success)
                        {
                            //tw.WriteLine(matchesValues0.Groups[1].Value + "\t" + matchesValues0.Groups[2].Value);
                            tw.WriteLine(matchesValues0.Groups[2].Value);
                            //tw.WriteLine(matchesValues0.Groups[1].Value + "\t" + matchesValues0.Groups[2].Value + "\t" + matchesValues0.Groups[3].Value + "\t" + matchesValues0.Groups[4].Value + "\t" + matchesValues0.Groups[5].Value + "\t" + matchesValues0.Groups[6].Value);
                            //tw.WriteLine(matchesValues0.Groups[2].Value + "\t" + matchesValues0.Groups[3].Value + "\t" + matchesValues0.Groups[6].Value);


                            matchesValues0 = matchesValues0.NextMatch();

                            //matchCount++;

                            //Debug.Log(matchCount);
                        }
                    }

                    //parse new file in a different thread here

                    readyForSimplifying2 = false;

                    _t2 = new Thread(() => _ParseNewFile(path));

                    _t2.Start();


                    //







                }
            }
            else
            {


                string currentFile = matchesValues0.Groups[1].Value;

                    matchesValues0 = matchesValues0.NextMatch();


                    while (currentFile == matchesValues0.Groups[1].Value)
                    {
                            matchesValues0 = matchesValues0.NextMatch();
                    }

                
            }

            


        }

        Debug.Log("finished splitting");
        Debug.Log("number of files: " + chosenFiles.Count);



        readyForSimplifying1 = true;


    }

    void _ParseNewFile(String filePath)
    {

        readyForSimplifying2 = false;

        float baselineValue;

        StreamReader readerNewFile = new StreamReader(filePath);
        Debug.Log("parsing new file " + filePath);

        string newFileContent;

        newFileContent = readerNewFile.ReadToEnd();

        readerNewFile.Close();

        File.Delete(filePath);

        // Debug.Log("new file content " + newFileContent);

        string patternValues = @"(-?[0-9]*.[0-9]*)\t(-?[0-9]*.[0-9]*)\t(-?[0-9]*.[0-9]*)\t(-?[0-9]*.[0-9]*)\t(-?[0-9]*.[0-9]*)"; //regex to get all values


        //

        Regex regexValues0 = new Regex(patternValues, RegexOptions.IgnoreCase);                                        //need to ignore the first (which is a duplicate of the final endlocation) and 
                                                                                                                       //get groups 2 and 4 as the latitude and longitude, respectively
        Match matchesValues0 = regexValues0.Match(newFileContent);


        baselineValue = float.Parse(matchesValues0.Groups[5].Value);    //first value

        while (matchesValues0.Success)
        {

            float currentVal = float.Parse(matchesValues0.Groups[5].Value);             //basing the baseline on PHI gain, not sure if this is right tho.

            if (currentVal < baselineValue)
            {
                baselineValue = currentVal;          //adjusting to the smallest one
            }


            matchesValues0 = matchesValues0.NextMatch();

        }

        Debug.Log("baseline defined as: " + baselineValue);

        baselineValue *= -1;                        //making it positive



        //
        string path = filePath;

        string currentFile = matchesValues0.Groups[1].Value;

        using (var tw = new StreamWriter(path, true))
        {




            Regex regexValues = new Regex(patternValues, RegexOptions.IgnoreCase);

            Match matchesValues = regexValues.Match(newFileContent);
            int matchCount = 1;


            while (matchesValues.Success)
            {


                // double freq = Double.Parse(matchesValues.Groups[1].Value);
                float phi = float.Parse(matchesValues.Groups[1].Value);
                float theta = float.Parse(matchesValues.Groups[2].Value);
                //ignoring phi and theta gains
                float gain = float.Parse(matchesValues.Groups[5].Value);

                gain = -(gain + baselineValue) / 10;

                float x = gain * Mathf.Sin(theta) * Mathf.Cos(phi);
                float y = gain *Mathf.Sin(theta) * Mathf.Sin(phi);
                float z = gain*Mathf.Cos(theta);

                //Debug.Log(x);

                tw.WriteLine(x + "\t" + y + "\t" + z + "\n");


                matchesValues = matchesValues.NextMatch();
                matchCount++;


                //errorTextField1.text = "n " + matchCount;







            }

            

            ///////



        }


        readyForSimplifying2 = true;

        //cartesianFileParser(filePath);
        //Debug.Log("parsing new cartesian file " + filePath);
    }


        // Update is called once per frame
        void Update()
    {


        if (readyForSimplifying1 && readyForSimplifying2)
        {
            readyForSimplifying1 = false;
            readyForSimplifying2 = false;

            fileSimplifier.SetActive(true);

        }

    }

    void cartesianFileParser(String parsedCartesianFile)
    {

        ///////////////         MIGHT NOT WORK ON THE HOLOLENS
        ///
        Debug.Log("starting cartesian file parsing " + parsedCartesianFile);

        StreamReader reader = new StreamReader(parsedCartesianFile);
        //Debug.Log(reader.ReadToEnd());

        string fileContent;

        fileContent = reader.ReadToEnd();

        Debug.Log("file read: " + fileContent);

        reader.Close();

        ////////////////

       // TextAsset dataAsset = Resources.Load(parsedCartesianFile) as TextAsset;     //filename without the frigging extension

        //Debug.Log(dataAsset);


        //Debug.Log(dataAsset);


        string patternValues = @"(.*)\t(.*)\t(.*)";         //regex to get all values

        //


        Regex regexValues = new Regex(patternValues, RegexOptions.IgnoreCase);

        Match matchesValues = regexValues.Match(fileContent);
        int matchCount = 1;


        while (matchesValues.Success)
        {
            Debug.Log("matched " + matchesValues.Groups[1].Value);

            Transform a;

            // double freq = Double.Parse(matchesValues.Groups[1].Value);

            Vector3 pos = new Vector3(float.Parse(matchesValues.Groups[1].Value), float.Parse(matchesValues.Groups[2].Value), float.Parse(matchesValues.Groups[3].Value));


            a = Instantiate(prefab, pos, Quaternion.identity, transform);
            a.localPosition = a.position;

            gameObject.GetComponent<LineRenderer>().positionCount = matchCount;
            gameObject.GetComponent<LineRenderer>().SetPosition(matchCount - 1, a.localPosition);


            Destroy(a.gameObject);

            matchesValues = matchesValues.NextMatch();
            matchCount++;


            errorTextField1.text = "n " + matchCount;
        }

        Debug.Log("done with matching");

        gameObject.GetComponent<LineRenderer>().startWidth = transform.localScale.x / 300f;

        float simplifyIndex = 0.1f;

        while (gameObject.GetComponent<LineRenderer>().positionCount > 2000)
        {
            gameObject.GetComponent<LineRenderer>().Simplify(simplifyIndex);

            simplifyIndex += 0.01f;
        }

        gameObject.GetComponent<LineRenderer>().positionCount--;       //removes the last (misplaced) point of the lines

        Debug.Log("simplified by " + simplifyIndex);



        // Debug.Log("simplificou");

        prefab.gameObject.SetActive(false);

        BoxCollider bc = gameObject.AddComponent<BoxCollider>();


    }



}

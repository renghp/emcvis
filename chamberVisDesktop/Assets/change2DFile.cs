﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class change2DFile : MonoBehaviour {

    public GameObject fileParser;
    public Text filePath;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void selectOtherFile()
    {
        if (filePath.text != "")
            fileParser.GetComponent<loadFileIntoString>().dataFileName = filePath.text;


        if (!fileParser.active)
            fileParser.SetActive(true);

        else
            fileParser.GetComponent<loadFileIntoString>().loadFile();

        gameObject.GetComponent<Button>().interactable = false;


    }
}

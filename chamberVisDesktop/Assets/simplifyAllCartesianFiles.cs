﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using System;
using System.IO;
using System.Text;
using UnityEngine.UI;

public class simplifyAllCartesianFiles : MonoBehaviour
{

    public Transform prefab;

    public String parsedCartesianFilePath;

    // Use this for initialization
    void Start()
    {


        DirectoryInfo dir = new DirectoryInfo(parsedCartesianFilePath);
        FileInfo[] info = dir.GetFiles("*.txt");
        foreach (FileInfo f in info)
        {


            cartesianFileParser(parsedCartesianFilePath + f.Name);
        }


        //cartesianFileParser(parsedCartesianFilePath);


    }

    // Update is called once per frame
    void Update()
    {

    }

    void cartesianFileParser(string parsedCartesianFile)
    {

        ///////////////         MIGHT NOT WORK ON THE HOLOLENS
        ///
       // Debug.Log("starting cartesian file parsing " + parsedCartesianFile);

        StreamReader reader = new StreamReader(parsedCartesianFile);
        //Debug.Log(reader.ReadToEnd());

        string fileContent;

        fileContent = reader.ReadToEnd();

        //Debug.Log("file read: " + fileContent);

        reader.Close();

        File.Delete(parsedCartesianFile);

        ////////////////    MIGHT NOT WORK ON THE HOLOLENS

        // TextAsset dataAsset = Resources.Load(parsedCartesianFile) as TextAsset;     //filename without the frigging extension

        //Debug.Log(dataAsset);


        //Debug.Log(dataAsset);


        string patternValues = @"(.*)\t(.*)\t(.*)";         //regex to get all values

        //


        Regex regexValues = new Regex(patternValues, RegexOptions.IgnoreCase);

        Match matchesValues = regexValues.Match(fileContent);
        int matchCount = 1;

        gameObject.GetComponent<LineRenderer>().positionCount = 0;      //erases the last lineRenderer;

        while (matchesValues.Success)
        {
            // Debug.Log("matched " + matchesValues.Groups[1].Value);

            Transform a;

            // double freq = Double.Parse(matchesValues.Groups[1].Value);

            Vector3 pos = new Vector3(float.Parse(matchesValues.Groups[1].Value), float.Parse(matchesValues.Groups[2].Value), float.Parse(matchesValues.Groups[3].Value));


            a = Instantiate(prefab, pos, Quaternion.identity, transform);
            a.localPosition = a.position;

            gameObject.GetComponent<LineRenderer>().positionCount = matchCount;
            gameObject.GetComponent<LineRenderer>().SetPosition(matchCount - 1, a.localPosition);


            Destroy(a.gameObject);

            matchesValues = matchesValues.NextMatch();
            matchCount++;


            // errorTextField1.text = "n " + matchCount;
        }

        //Debug.Log("done with matching");

        gameObject.GetComponent<LineRenderer>().startWidth = transform.localScale.x / 600f;

        float simplifyIndex = 0.1f;

        while (gameObject.GetComponent<LineRenderer>().positionCount > 2000)
        {
            gameObject.GetComponent<LineRenderer>().Simplify(simplifyIndex);

            simplifyIndex += 0.01f;
        }

        gameObject.GetComponent<LineRenderer>().positionCount--;       //removes the last (misplaced) point of the lines

        prefab.gameObject.SetActive(false);


        ////////////////// write new positions into file
        ///
        int iterator = 0;

        using (var tw = new StreamWriter(parsedCartesianFile, true))
        {


            while (iterator < gameObject.GetComponent<LineRenderer>().positionCount)
            {


                //gameObject.GetComponent<LineRenderer>().SetPosition(matchCount - 1, a.localPosition);

                Vector3 currentPos = gameObject.GetComponent<LineRenderer>().GetPosition(iterator);


                //Debug.Log(x);

                tw.WriteLine(currentPos.x + "\t" + currentPos.y + "\t" + currentPos.z + "\n");





                //errorTextField1.text = "n " + matchCount;





                iterator++;

            }



            ///////



        }


        /////////////////////////

        gameObject.GetComponent<LineRenderer>().positionCount = 0;

        //BoxCollider bc = gameObject.AddComponent<BoxCollider>();


    }
}

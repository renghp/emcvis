﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using System;
using System.IO;
using System.Text;
using UnityEngine.UI;




public class postParsingDataController : MonoBehaviour
{


    private Vector3 originalPos;
    private Vector3 originalRot;
    private Vector3 originalScale;

    public GameObject threeDFileDataN1;
    public GameObject threeDFileDataN2;
    public GameObject twoDFileData;

    public GameObject canvasDefault;

    public GameObject canvasScale;
    public GameObject canvasRotation;
    public GameObject canvasTransUpDown;
    public GameObject canvasTransLeftRight;
    public GameObject canvasTransForBack;


    public Text errorTextField1;
    public Text errorTextField2;

    public GameObject menu;
    public GameObject hoveringInstructions;

    public Transform subtitlesFather;

    public GameObject cursor;

    bool menuOpen = true;

    bool scaling = false;
    bool rotating = false;
    bool translatingForBack = false;
    bool translatingUpDown = false;
    bool translatingLeftRight = false;

    private Vector3 pointFrozen;

    private float lastRotY;
    private float lastScale;
    private float lastPosForBack;
    private float lastPosUpDown;
    private float lastPosLeftRight;

    public Transform hand;

    public Material dataMat, dataMat2, scalingMat;


    private float scaleMin;

    public

    // Use this for initialization
    void Start()
    {

        originalPos = transform.localPosition;
        originalRot = transform.localEulerAngles;
        originalScale = transform.localScale;

        menu.SetActive(menuOpen);
        hoveringInstructions.SetActive(menuOpen);


        canvasDefault.SetActive(true);

        canvasScale.SetActive(false);
        canvasRotation.SetActive(false);
        canvasTransUpDown.SetActive(false);
        canvasTransLeftRight.SetActive(false);
        canvasTransForBack.SetActive(false);

        scaleMin = 0.6f;

        threeDFileDataN1.GetComponent<LineRenderer>().material = dataMat;
        threeDFileDataN2.GetComponent<LineRenderer>().material = dataMat2;
        twoDFileData.GetComponent<LineRenderer>().material = dataMat;

        //gameObject.GetComponent<LineRenderer>().alignment = LineAlignment.TransformZ;



    }

    

    // Update is called once per frame
    void Update()
    {

        if (scaling)
        {

            float dist = Vector3.Distance(pointFrozen, hand.localPosition) * 4f;

            //errorTextField1.text = "dist: "+dist.ToString();

            if (hand.localPosition.x > pointFrozen.x)
            {
                transform.localScale = new Vector3(lastScale + dist, lastScale + dist, lastScale + dist);
            }
            else
            {
                transform.localScale = new Vector3(lastScale - dist, lastScale - dist, lastScale - dist);
            }

            if (transform.localScale.x < scaleMin)
            {
                transform.localScale = new Vector3(scaleMin, scaleMin, scaleMin);
            }


            threeDFileDataN1.GetComponent<LineRenderer>().startWidth = threeDFileDataN1.transform.localScale.x / 100f;
            threeDFileDataN2.GetComponent<LineRenderer>().startWidth = threeDFileDataN2.transform.localScale.x / 100f;
            twoDFileData.GetComponent<LineRenderer>().startWidth = twoDFileData.transform.localScale.x / 30f;
        }
        else if (rotating)
        {

            float dist = Vector3.Distance(pointFrozen, hand.localPosition) * 200f;

            //errorTextField1.text = "dist: "+dist.ToString();

            if (hand.localPosition.x > pointFrozen.x)
            {
                transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, lastRotY + dist, transform.localEulerAngles.z);
            }
            else
            {
                transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, lastRotY - dist, transform.localEulerAngles.z);
            }


        }
        else if (translatingForBack)
        {
            float dist = Vector3.Distance(pointFrozen, hand.localPosition) * 1.5f;

            //errorTextField1.text = "dist: "+dist.ToString();

            if (hand.localPosition.x > pointFrozen.x)
            {
                transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, lastPosForBack - dist);
            }
            else
            {
                transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, lastPosForBack + dist);
            }
        }
        else if (translatingLeftRight)
        {
            float dist = Vector3.Distance(pointFrozen, hand.localPosition) * 1.5f;

            //errorTextField1.text = "dist: "+dist.ToString();

            if (hand.localPosition.x > pointFrozen.x)
            {
                transform.localPosition = new Vector3(lastPosLeftRight - dist, transform.localPosition.y, transform.localPosition.z);
            }
            else
            {
                transform.localPosition = new Vector3(lastPosLeftRight + dist, transform.localPosition.y, transform.localPosition.z);
            }
        }
        else if (translatingUpDown)
        {
            float dist = Vector3.Distance(pointFrozen, hand.localPosition) * 1.5f;

            //errorTextField1.text = "dist: "+dist.ToString();

            if (hand.localPosition.x > pointFrozen.x)
            {
                transform.localPosition = new Vector3(transform.localPosition.x, lastPosUpDown + dist, transform.localPosition.z);
            }
            else
            {
                transform.localPosition = new Vector3(transform.localPosition.x, lastPosUpDown - dist, transform.localPosition.z);
            }
        }



    }

    void OnSelect()
    {
        if (menuOpen)
        {
            if (scaling || rotating || translatingForBack || translatingUpDown || translatingLeftRight)
            {

                threeDFileDataN1.GetComponent<LineRenderer>().material = dataMat;
                threeDFileDataN2.GetComponent<LineRenderer>().material = dataMat2;
                twoDFileData.GetComponent<LineRenderer>().material = dataMat;

                scaling = false;
                rotating = false;
                translatingForBack = false;
                translatingUpDown = false;
                translatingLeftRight = false;


                canvasDefault.SetActive(true);

                canvasScale.SetActive(false);
                canvasRotation.SetActive(false);
                canvasTransUpDown.SetActive(false);
                canvasTransLeftRight.SetActive(false);
                canvasTransForBack.SetActive(false);

                cursor.GetComponent<worldCursor>().selectedOff();
            }
            else
            {
                menuOpen = false;
            }
        }
        else
        {
            menuOpen = true;
        }



        menu.SetActive(menuOpen);
        hoveringInstructions.SetActive(menuOpen);
    }

    public void resetTransform ()
    {
        transform.localPosition = originalPos;
        transform.localEulerAngles = originalRot;
        transform.localScale = originalScale;
    }

    public void translationSelected(int direction)
    {


        switch (direction)
        {
            case 1:
            case 2:

                translatingForBack = !translatingForBack;


                if (translatingForBack)
                {
                    scaling = false;
                    rotating = false;
                    translatingUpDown = false;
                    translatingLeftRight = false;

                    canvasTransForBack.SetActive(true);

                    canvasDefault.SetActive(false);
                    canvasScale.SetActive(false);
                    canvasTransUpDown.SetActive(false);
                    canvasTransLeftRight.SetActive(false);
                    canvasRotation.SetActive(false);

                    threeDFileDataN1.GetComponent<LineRenderer>().material = scalingMat;
                    threeDFileDataN2.GetComponent<LineRenderer>().material = scalingMat;
                    twoDFileData.GetComponent<LineRenderer>().material = scalingMat;

                    lastPosForBack = transform.localPosition.z;

                    pointFrozen = hand.localPosition;




                }
                else
                {
                    threeDFileDataN1.GetComponent<LineRenderer>().material = dataMat;
                    threeDFileDataN2.GetComponent<LineRenderer>().material = dataMat2;
                    twoDFileData.GetComponent<LineRenderer>().material = dataMat;

                    cursor.GetComponent<worldCursor>().selectedOff();

                    canvasDefault.SetActive(true);

                    canvasScale.SetActive(false);
                    canvasRotation.SetActive(false);
                    canvasTransUpDown.SetActive(false);
                    canvasTransLeftRight.SetActive(false);
                    canvasTransForBack.SetActive(false);

                    //errorTextField1.text = "parou de escalar";
                    //fatherObject.parent = originalParent;
                    // fatherObject.localEulerAngles = new Vector3(0f, fatherObject.localEulerAngles.y, 0f);     //preserves the pitch and roll rotations (hololens takes care of them), only changing the yaw

                }
                //data.localPosition = new Vector3(data.localPosition.x, data.localPosition.y, data.localPosition.z - transIncrease);


                break;

            case 3:
            case 4:

                translatingLeftRight = !translatingLeftRight;

                if (translatingLeftRight)
                {
                    scaling = false;
                    rotating = false;
                    translatingUpDown = false;
                    translatingForBack = false;

                    canvasTransLeftRight.SetActive(true);

                    canvasDefault.SetActive(false);
                    canvasScale.SetActive(false);
                    canvasTransUpDown.SetActive(false);
                    canvasTransForBack.SetActive(false);
                    canvasRotation.SetActive(false);

                    threeDFileDataN1.GetComponent<LineRenderer>().material = scalingMat;
                    threeDFileDataN2.GetComponent<LineRenderer>().material = scalingMat;
                    twoDFileData.GetComponent<LineRenderer>().material = scalingMat;

                    lastPosLeftRight = transform.localPosition.x;

                    pointFrozen = hand.localPosition;




                }
                else
                {
                    threeDFileDataN1.GetComponent<LineRenderer>().material = dataMat;
                    threeDFileDataN2.GetComponent<LineRenderer>().material = dataMat2;
                    twoDFileData.GetComponent<LineRenderer>().material = dataMat;

                    cursor.GetComponent<worldCursor>().selectedOff();

                    canvasDefault.SetActive(true);

                    canvasScale.SetActive(false);
                    canvasRotation.SetActive(false);
                    canvasTransUpDown.SetActive(false);
                    canvasTransLeftRight.SetActive(false);
                    canvasTransForBack.SetActive(false);

                    //errorTextField1.text = "parou de escalar";
                    //fatherObject.parent = originalParent;
                    // fatherObject.localEulerAngles = new Vector3(0f, fatherObject.localEulerAngles.y, 0f);     //preserves the pitch and roll rotations (hololens takes care of them), only changing the yaw

                }
                //data.localPosition = new Vector3(data.localPosition.x - transIncrease, data.localPosition.y, data.localPosition.z);


                break;

            case 5:
            case 6:

                translatingUpDown = !translatingUpDown;

                if (translatingUpDown)
                {
                    scaling = false;
                    rotating = false;
                    translatingLeftRight = false;
                    translatingForBack = false;

                    canvasTransUpDown.SetActive(true);

                    canvasDefault.SetActive(false);
                    canvasScale.SetActive(false);
                    canvasTransLeftRight.SetActive(false);
                    canvasTransForBack.SetActive(false);
                    canvasRotation.SetActive(false);

                    threeDFileDataN1.GetComponent<LineRenderer>().material = scalingMat;
                    threeDFileDataN2.GetComponent<LineRenderer>().material = scalingMat;
                    twoDFileData.GetComponent<LineRenderer>().material = scalingMat;

                    lastPosUpDown = transform.localPosition.y;

                    pointFrozen = hand.localPosition;




                }
                else
                {
                    threeDFileDataN1.GetComponent<LineRenderer>().material = dataMat;
                    threeDFileDataN2.GetComponent<LineRenderer>().material = dataMat2;
                    twoDFileData.GetComponent<LineRenderer>().material = dataMat;

                    cursor.GetComponent<worldCursor>().selectedOff();

                    canvasDefault.SetActive(true);

                    canvasScale.SetActive(false);
                    canvasRotation.SetActive(false);
                    canvasTransUpDown.SetActive(false);
                    canvasTransLeftRight.SetActive(false);
                    canvasTransForBack.SetActive(false);

                    //errorTextField1.text = "parou de escalar";
                    //fatherObject.parent = originalParent;
                    // fatherObject.localEulerAngles = new Vector3(0f, fatherObject.localEulerAngles.y, 0f);     //preserves the pitch and roll rotations (hololens takes care of them), only changing the yaw

                }
                //data.localPosition = new Vector3(data.localPosition.x, data.localPosition.y + transIncrease, data.localPosition.z);


                break;

        }
    }

    public void rotSelected()
    {


        rotating = !rotating;



        if (rotating)
        {
            scaling = false;
            translatingForBack = false;
            translatingUpDown = false;
            translatingLeftRight = false;

            canvasRotation.SetActive(true);

            canvasDefault.SetActive(false);
            canvasScale.SetActive(false);
            canvasTransUpDown.SetActive(false);
            canvasTransLeftRight.SetActive(false);
            canvasTransForBack.SetActive(false);

            threeDFileDataN1.GetComponent<LineRenderer>().material = scalingMat;
            threeDFileDataN2.GetComponent<LineRenderer>().material = scalingMat;
            twoDFileData.GetComponent<LineRenderer>().material = scalingMat;

            lastRotY = transform.localEulerAngles.y;

            pointFrozen = hand.localPosition;




        }
        else
        {
            threeDFileDataN1.GetComponent<LineRenderer>().material = dataMat;
            threeDFileDataN2.GetComponent<LineRenderer>().material = dataMat2;
            twoDFileData.GetComponent<LineRenderer>().material = dataMat;

            cursor.GetComponent<worldCursor>().selectedOff();

            canvasDefault.SetActive(true);

            canvasScale.SetActive(false);
            canvasRotation.SetActive(false);
            canvasTransUpDown.SetActive(false);
            canvasTransLeftRight.SetActive(false);
            canvasTransForBack.SetActive(false);

            //errorTextField1.text = "parou de escalar";
            //fatherObject.parent = originalParent;
            // fatherObject.localEulerAngles = new Vector3(0f, fatherObject.localEulerAngles.y, 0f);     //preserves the pitch and roll rotations (hololens takes care of them), only changing the yaw

        }



    }

    public void scaleSelected()
    {


        scaling = !scaling;



        if (scaling)
        {
            rotating = false;
            translatingForBack = false;
            translatingUpDown = false;
            translatingLeftRight = false;

            canvasScale.SetActive(true);

            canvasDefault.SetActive(false);
            canvasRotation.SetActive(false);
            canvasTransUpDown.SetActive(false);
            canvasTransLeftRight.SetActive(false);
            canvasTransForBack.SetActive(false);

            /* foreach (GameObject child in subtitlesFather)
             {
                 child.SetActive(false);
             }*/

            //subtitlesFather.GetChild(0).gameObject.SetActive(true);

            threeDFileDataN1.GetComponent<LineRenderer>().material = scalingMat;
            threeDFileDataN2.GetComponent<LineRenderer>().material = scalingMat;
            twoDFileData.GetComponent<LineRenderer>().material = scalingMat;

            lastScale = transform.localScale.x;
            //errorTextField1.text = "vai escalar";
            pointFrozen = hand.localPosition;

            // fatherObject.parent = transform;
            // fatherObject.parent = Camera.main.transform;


        }
        else
        {
            threeDFileDataN1.GetComponent<LineRenderer>().material = dataMat;
            threeDFileDataN2.GetComponent<LineRenderer>().material = dataMat2;
            twoDFileData.GetComponent<LineRenderer>().material = dataMat;

            cursor.GetComponent<worldCursor>().selectedOff();

            canvasDefault.SetActive(true);

            canvasScale.SetActive(false);
            canvasRotation.SetActive(false);
            canvasTransUpDown.SetActive(false);
            canvasTransLeftRight.SetActive(false);
            canvasTransForBack.SetActive(false);

            //errorTextField1.text = "parou de escalar";
            //fatherObject.parent = originalParent;
            // fatherObject.localEulerAngles = new Vector3(0f, fatherObject.localEulerAngles.y, 0f);     //preserves the pitch and roll rotations (hololens takes care of them), only changing the yaw

        }



    }



}

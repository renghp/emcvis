﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class startParsing : MonoBehaviour {

    public GameObject fileParser;
    public Text filePath;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void turnOnParser()
    {
        if (filePath.text != "")
            fileParser.GetComponent<parse3DFileWithoutSplit>().dataFileName = filePath.text;


        if (!fileParser.active)
            fileParser.SetActive(true);

        else
            fileParser.GetComponent<parse3DFileWithoutSplit>().Start();

        gameObject.GetComponent<Button>().interactable = false;


    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.IO;
using System.Text;
using UnityEngine.UI;


public class loadFileIntoString : MonoBehaviour {


    public string dataFileName;
    public string fileContent;

    // Use this for initialization
    void Start () {

        loadFile();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void loadFile()
    {
        StreamReader reader = new StreamReader(dataFileName);
        //Debug.Log(reader.ReadToEnd());



        fileContent = reader.ReadToEnd();

        reader.Close();

        gameObject.GetComponent<Text>().text = "File " + dataFileName + " ready";
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloLensHandTracking;

public class chairSwitchController : MonoBehaviour
{

    public Transform placeHolder;

    private bool status;

    public GameObject virtualSeats;

    // Use this for initialization
    void Start()
    {

        status = true;
       // OnSelect();

    }

    // Update is called once per frame
    void Update()
    {

        transform.position = placeHolder.position;
        transform.eulerAngles = placeHolder.eulerAngles;

    }

    void OnSelect()
    {

        
        status = !status;

        virtualSeats.GetComponent<seatScores67>().switchChairs(status);

    }
}



﻿using UnityEngine;
using UnityEngine.UI;

public class worldCursor : MonoBehaviour
{
    private MeshRenderer meshRenderer;

    public GameObject[] attButtons;

    public GameObject canvasDB;
    public Text dbTextValue;

    // Use this for initialization
    void Start()
    {
        // Grab the mesh renderer that's on the same object as this script.
        meshRenderer = this.gameObject.GetComponentInChildren<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        // Do a raycast into the world based on the user's
        // head position and orientation.
        var headPosition = Camera.main.transform.position;
        var gazeDirection = Camera.main.transform.forward;

        

        RaycastHit hitInfo;

        Transform objHit;

        if (Physics.Raycast(headPosition, gazeDirection, out hitInfo))
        {
            // If the raycast hits a hologram
            // Display the cursor mesh.
            meshRenderer.enabled = true;

            // Move the cursor to the point where the raycast hit. 
            this.transform.position = hitInfo.point;

            objHit = hitInfo.transform;

            // Rotate the cursor to hug the surface of the hologram.
            this.transform.rotation = Quaternion.FromToRotation(Vector3.up, hitInfo.normal);

            if (objHit.tag == "attButton")
            {

                canvasDB.SetActive(false);

                //attButtons = GameObject.FindGameObjectsWithTag("attButton");

                foreach (GameObject aB in attButtons)
                {
                    aB.GetComponent<changeAttribute>().unHit();
                }

                objHit.GetComponent<changeAttribute>().cursorHit();
            }

            else if (objHit.tag == "2DRay")
            {
                foreach (Transform child in objHit.parent)
                {
                    child.GetComponent<LineRenderer>().startWidth = 0.01f;
                    child.GetComponent<LineRenderer>().endWidth = 0.01f;
                }

                objHit.GetComponent<LineRenderer>().startWidth = 0.05f;
                objHit.GetComponent<LineRenderer>().endWidth = 0.05f;

                canvasDB.SetActive(true);
                //dbTextValue.text = objHit.GetComponent<Text>().text + " dB";
            }

            else
            {
                canvasDB.SetActive(false);
            }

        }
        else
        {
            // If the raycast did not hit a hologram, hide the cursor mesh.
            meshRenderer.enabled = false;
        }


    }

    public void selectedOff()
    {
        foreach (GameObject aB in attButtons)
        {
            aB.GetComponent<changeAttribute>().selectedOff();
        }
    }

}
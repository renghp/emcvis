﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using System;
using System.IO;
using System.Text;
using UnityEngine.UI;




public class parse2DcolorCodedPlane : MonoBehaviour
{

    public Text feedbackLoading;

    public string dataFileName;

    public bool dataIs2D;

    public GameObject canvasDefault;

    public GameObject canvasScale;
    public GameObject canvasRotation;
    public GameObject canvasTransUpDown;
    public GameObject canvasTransLeftRight;
    public GameObject canvasTransForBack;

    public Transform prefab;

    public Text errorTextField1;
    public Text errorTextField2;

    public GameObject menu;

    public Transform subtitlesFather;

    public GameObject cursor;

    bool menuOpen = true;

    bool scaling = false;
    bool rotating = false;
    bool translatingForBack = false;
    bool translatingUpDown = false;
    bool translatingLeftRight = false;

    private Vector3 pointFrozen;

    private float lastRotY;
    private float lastScale;
    private float lastPosForBack;
    private float lastPosUpDown;
    private float lastPosLeftRight;

    public Transform hand;

    public Material dataMat, scalingMat;

    private float baselineValue;
    private float toplineValue;

    private float scaleMin;

    public String fileFromServer;

    public

    // Use this for initialization
    void Start()
    {
        menu.SetActive(menuOpen);


        canvasDefault.SetActive(true);

        canvasScale.SetActive(false);
        canvasRotation.SetActive(false);
        canvasTransUpDown.SetActive(false);
        canvasTransLeftRight.SetActive(false);
        canvasTransForBack.SetActive(false);

        scaleMin = 0.3f;

        gameObject.GetComponent<LineRenderer>().material = dataMat;

        //gameObject.GetComponent<LineRenderer>().alignment = LineAlignment.TransformZ;




        twoDFileParserFromServer();





    }


    public void twoDFileParserFromServer()
    {
        
        string patternValues = @"(-?[0-9]*.[0-9]*e(\+|-)[0-9]*) ((-)?[0-9]*.[0-9]*e(\+|-)[0-9]*)"; //regex to get all values

        //

        Regex regexValues0 = new Regex(patternValues, RegexOptions.IgnoreCase);                                        //need to ignore the first (which is a duplicate of the final endlocation) and 


        try
        {
            Match matchesValues0 = regexValues0.Match(fileFromServer);

            baselineValue = (float)Double.Parse(matchesValues0.Groups[3].Value);    //first value
            toplineValue = (float)Double.Parse(matchesValues0.Groups[3].Value);    //first value

            while (matchesValues0.Success)
            {
                //Debug.Log("baseline defined as: " + baselineValue);
                //Debug.Log("valor 1: "+matchesValues.Groups[1].Value);
                //Debug.Log("valor 2: " + matchesValues.Groups[3].Value);

                float currentVal = (float)Double.Parse(matchesValues0.Groups[3].Value);

                if (currentVal < baselineValue)
                {
                    baselineValue = currentVal;          //adjusting to the smallest one
                }

                if (currentVal > toplineValue)
                {
                    toplineValue = currentVal;          //adjusting to the biggest one
                }


                matchesValues0 = matchesValues0.NextMatch();

            }

            Debug.Log("baseline defined as: " + baselineValue);

            baselineValue *= -1;                        //making it positive



            //


            Regex regexValues = new Regex(patternValues, RegexOptions.IgnoreCase);

            Match matchesValues = regexValues.Match(fileFromServer);
            int matchCount = 1;


            while (matchesValues.Success)
            {

                if (matchCount % 3 == 0)
                {

                Transform a;

                    a = Instantiate(prefab, SphericalToCartesian2D((float)Double.Parse(matchesValues.Groups[1].Value), (float)Double.Parse(matchesValues.Groups[3].Value)), Quaternion.identity, transform);
                    a.localPosition = new Vector3(a.position.x, a.position.y, a.position.z);

                    a.LookAt(transform);
                    a.localEulerAngles = new Vector3(a.localEulerAngles.x, a.localEulerAngles.y + 180f, a.localEulerAngles.z);

                    float percent = (1 / (toplineValue - ((baselineValue * -1))) * (float)Double.Parse(matchesValues.Groups[3].Value)) - ((baselineValue * -1) / (toplineValue - (baselineValue * -1)));
                    Debug.Log(percent);

                float wavelength = (1 / (toplineValue - ((baselineValue * -1))) * (float)Double.Parse(matchesValues.Groups[3].Value)) - ((baselineValue * -1) / (toplineValue - (baselineValue * -1)));


                Color currentColor;

                float R, G, B, attenuation;
                float gamma = 0.8f;



                if (wavelength >= 0 & wavelength <= 0.1621f)
                {
                    attenuation = 0.3f + 0.7f * (wavelength - 0) / (0.1621f - 0);
                    R = (float)Math.Pow(((-(wavelength - 0.1621f) / (0.1621f - 0)) * attenuation), gamma);
                    G = 0.0f;
                    B = (float)Math.Pow((1.0 * attenuation), gamma);
                }
                else if (wavelength >= 0.1621f & wavelength <= 0.2972f)
                {
                    R = 0.0f;
                    G = (float)Math.Pow(((wavelength - 0.1621f) / (0.2972f - 0.1621f)), gamma);
                    B = 1.0f;
                }
                else if (wavelength >= 0.2972f & wavelength <= 0.3513f)
                {
                    R = 0.0f;
                    G = 1.0f;
                    B = (float)Math.Pow((-(wavelength - 0.3513f) / (0.3513f - 0.2972f)), gamma);
                }
                else if (wavelength >= 0.3513f & wavelength <= 0.5405f)
                {
                    R = (float)Math.Pow(((wavelength - 0.3513f) / (0.5405f - 0.3513f)), gamma);
                    G = 1.0f;
                    B = 0.0f;
                }
                else if (wavelength >= 0.5405f & wavelength <= 0.7162f)
                {
                    R = 1.0f;
                    G = (float)Math.Pow((-(wavelength - 0.7162f) / (0.7162f - 0.5405f)), gamma);
                    B = 0.0f;
                }
                else if (wavelength >= 0.7162f & wavelength <= 1f)
                {
                    attenuation = 0.3f + 0.7f * (1f - wavelength) / (1f - 0.7162f);
                    R = (float)Math.Pow((1.0f * attenuation), gamma);
                    G = 0.0f;
                    B = 0.0f;
                }
                else
                {
                    R = 0.0f;
                    G = 0.0f;
                    B = 0.0f;
                }


                currentColor = new Color(R, G, B, 1f);
                Color finalColor = new Color(R, G, B, 0f);

                a.GetComponent<LineRenderer>().SetColors(currentColor, finalColor);


                }
                matchesValues = matchesValues.NextMatch();
                matchCount++;
            }



            Debug.Log("deu boa, pegou arquivo online");
            feedbackLoading.text = "File loaded successfully.";

            prefab.gameObject.SetActive(false);

            BoxCollider bc = gameObject.AddComponent<BoxCollider>();
        }
        catch
        {
            Debug.Log("deu ruim, pegando arquivo offline");

            feedbackLoading.text = "Problems with the server file, loading offline file instead.";
            twoDFileParser();
        }


        Debug.Log(fileFromServer);


       
    }


    void twoDFileParser()
    {

        TextAsset dataAsset = Resources.Load(dataFileName) as TextAsset;     //filename without the frigging extension

        Debug.Log(dataAsset);


        string patternValues = @"(-?[0-9]*.[0-9]*e(\+|-)[0-9]*) ((-)?[0-9]*.[0-9]*e(\+|-)[0-9]*)"; //regex to get all values

        //

        Regex regexValues0 = new Regex(patternValues, RegexOptions.IgnoreCase);                                        //need to ignore the first (which is a duplicate of the final endlocation) and 
                                                                                                                       //get groups 2 and 4 as the latitude and longitude, respectively
        Match matchesValues0 = regexValues0.Match(dataAsset.text);


        baselineValue = (float)Double.Parse(matchesValues0.Groups[3].Value);    //first value
        toplineValue = (float)Double.Parse(matchesValues0.Groups[3].Value);    //first value

        while (matchesValues0.Success)
        {
            //Debug.Log("baseline defined as: " + baselineValue);
            //Debug.Log("valor 1: "+matchesValues.Groups[1].Value);
            //Debug.Log("valor 2: " + matchesValues.Groups[3].Value);

            float currentVal = (float)Double.Parse(matchesValues0.Groups[3].Value);

            if (currentVal < baselineValue)
            {
                baselineValue = currentVal;          //adjusting to the smallest one
            }

            if (currentVal > toplineValue)
            {
                toplineValue = currentVal;          //adjusting to the biggest one
            }


            matchesValues0 = matchesValues0.NextMatch();

        }

        Debug.Log("baseline defined as: " + baselineValue);

        baselineValue *= -1;                        //making it positive



        //


        Regex regexValues = new Regex(patternValues, RegexOptions.IgnoreCase);                                        
                                                                                                                     
        Match matchesValues = regexValues.Match(dataAsset.text);
        int matchCount = 1;


        while (matchesValues.Success)
        {

            if (matchCount%3 == 0)
            { 

                Transform a;

                a = Instantiate(prefab, SphericalToCartesian2D((float)Double.Parse(matchesValues.Groups[1].Value), (float)Double.Parse(matchesValues.Groups[3].Value)), Quaternion.identity, transform);
                a.localPosition = new Vector3(a.position.x, a.position.y, a.position.z);

         
                a.LookAt(transform);
                a.localEulerAngles = new Vector3(a.localEulerAngles.x, a.localEulerAngles.y + 180f, a.localEulerAngles.z);

                a.gameObject.GetComponent<Text>().text = ((float)Double.Parse(matchesValues.Groups[3].Value)).ToString("0.###");

                float wavelength = (1 / (toplineValue - ((baselineValue * -1))) * (float)Double.Parse(matchesValues.Groups[3].Value) ) - ((baselineValue*-1) / (toplineValue - (baselineValue * -1)));


                Color currentColor;

                float R, G, B, attenuation;
                float gamma = 0.8f;



                if (wavelength >= 0 & wavelength <= 0.1621f)
                {
                    attenuation = 0.3f + 0.7f * (wavelength - 0) / (0.1621f - 0);
                    R = (float)Math.Pow(((-(wavelength - 0.1621f) / (0.1621f - 0)) * attenuation), gamma);
                    G = 0.0f;
                    B = (float)Math.Pow((1.0 * attenuation), gamma);
                }
                else if (wavelength >= 0.1621f & wavelength <= 0.2972f)
                {
                    R = 0.0f;
                    G = (float)Math.Pow(((wavelength - 0.1621f) / (0.2972f - 0.1621f)), gamma);
                    B = 1.0f;
                }
                else if (wavelength >= 0.2972f & wavelength <= 0.3513f)
                {
                    R = 0.0f;
                    G = 1.0f;
                    B = (float)Math.Pow((-(wavelength - 0.3513f) / (0.3513f - 0.2972f)), gamma);
                }
                else if (wavelength >= 0.3513f & wavelength <= 0.5405f)
                {
                    R = (float)Math.Pow(((wavelength - 0.3513f) / (0.5405f - 0.3513f)), gamma);
                    G = 1.0f;
                    B = 0.0f;
                }
                else if (wavelength >= 0.5405f & wavelength <= 0.7162f)
                {
                    R = 1.0f;
                    G = (float)Math.Pow((-(wavelength - 0.7162f) / (0.7162f - 0.5405f)), gamma);
                    B = 0.0f;
                }
                else if (wavelength >= 0.7162f & wavelength <= 1f)
                {
                    attenuation = 0.3f + 0.7f * (1f - wavelength) / (1f - 0.7162f);
                    R = (float)Math.Pow((1.0f * attenuation), gamma);
                    G = 0.0f;
                    B = 0.0f;
                }
                else
                {
                    R = 0.0f;
                    G = 0.0f;
                    B = 0.0f;
                }


                currentColor = new Color(R, G, B, 1f);
                Color finalColor = new Color(R, G, B, 0f);

                a.GetComponent<LineRenderer>().SetColors(currentColor, finalColor);


            }
                matchesValues = matchesValues.NextMatch();
                matchCount++;
        }

            float simplifyIndex = 0.1f;




            prefab.gameObject.SetActive(false);

            BoxCollider bc = gameObject.AddComponent<BoxCollider>();
            bc.center = new Vector3(-0.4f, 0f, 0f);
        }

        // Update is called once per frame
        void Update()
        {

            if (scaling)
            {

                float dist = Vector3.Distance(pointFrozen, hand.localPosition) * 4f;

                //errorTextField1.text = "dist: "+dist.ToString();

                if (hand.localPosition.x > pointFrozen.x)
                {
                    transform.localScale = new Vector3(lastScale + dist, transform.localScale.y, lastScale + dist);
                }
                else
                {
                    transform.localScale = new Vector3(lastScale - dist, transform.localScale.y, lastScale - dist);
                }

                if (transform.localScale.x < scaleMin)
                {
                    transform.localScale = new Vector3(scaleMin, transform.localScale.y, scaleMin);
                }


                gameObject.GetComponent<LineRenderer>().startWidth = transform.localScale.x / 30f;
                // gameObject.GetComponent<LineRenderer>().endWidth = transform.localScale.x / 30f;
            }
            else if (rotating)
            {

                float dist = Vector3.Distance(pointFrozen, hand.localPosition) * 200f;

                //errorTextField1.text = "dist: "+dist.ToString();

                if (hand.localPosition.x > pointFrozen.x)
                {
                    transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, lastRotY + dist, transform.localEulerAngles.z);
                }
                else
                {
                    transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, lastRotY - dist, transform.localEulerAngles.z);
                }


            }
            else if (translatingForBack)
            {
                float dist = Vector3.Distance(pointFrozen, hand.localPosition) * 1.5f;

                //errorTextField1.text = "dist: "+dist.ToString();

                if (hand.localPosition.x > pointFrozen.x)
                {
                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, lastPosForBack - dist);
                }
                else
                {
                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, lastPosForBack + dist);
                }
            }
            else if (translatingLeftRight)
            {
                float dist = Vector3.Distance(pointFrozen, hand.localPosition) * 1.5f;

                //errorTextField1.text = "dist: "+dist.ToString();

                if (hand.localPosition.x > pointFrozen.x)
                {
                    transform.localPosition = new Vector3(lastPosLeftRight - dist, transform.localPosition.y, transform.localPosition.z);
                }
                else
                {
                    transform.localPosition = new Vector3(lastPosLeftRight + dist, transform.localPosition.y, transform.localPosition.z);
                }
            }
            else if (translatingUpDown)
            {
                float dist = Vector3.Distance(pointFrozen, hand.localPosition) * 1.5f;

                //errorTextField1.text = "dist: "+dist.ToString();

                if (hand.localPosition.x > pointFrozen.x)
                {
                    transform.localPosition = new Vector3(transform.localPosition.x, lastPosUpDown + dist, transform.localPosition.z);
                }
                else
                {
                    transform.localPosition = new Vector3(transform.localPosition.x, lastPosUpDown - dist, transform.localPosition.z);
                }
            }



        }

        void OnSelect()
        {
            if (menuOpen)
            {
                if (scaling || rotating || translatingForBack || translatingUpDown || translatingLeftRight)
                {

                    gameObject.GetComponent<LineRenderer>().material = dataMat;

                    scaling = false;
                    rotating = false;
                    translatingForBack = false;
                    translatingUpDown = false;
                    translatingLeftRight = false;


                    canvasDefault.SetActive(true);

                    canvasScale.SetActive(false);
                    canvasRotation.SetActive(false);
                    canvasTransUpDown.SetActive(false);
                    canvasTransLeftRight.SetActive(false);
                    canvasTransForBack.SetActive(false);

                    cursor.GetComponent<worldCursor>().selectedOff();
                }
                else
                {
                    menuOpen = false;
                }
            }
            else
            {
                menuOpen = true;
            }



            menu.SetActive(menuOpen);
        }

        public void translationSelected(int direction)
        {


            switch (direction)
            {
                case 1:
                case 2:

                    translatingForBack = !translatingForBack;


                    if (translatingForBack)
                    {
                        scaling = false;
                        rotating = false;
                        translatingUpDown = false;
                        translatingLeftRight = false;

                        canvasTransForBack.SetActive(true);

                        canvasDefault.SetActive(false);
                        canvasScale.SetActive(false);
                        canvasTransUpDown.SetActive(false);
                        canvasTransLeftRight.SetActive(false);
                        canvasRotation.SetActive(false);

                        gameObject.GetComponent<LineRenderer>().material = scalingMat;

                        lastPosForBack = transform.localPosition.z;

                        pointFrozen = hand.localPosition;




                    }
                    else
                    {
                        gameObject.GetComponent<LineRenderer>().material = dataMat;

                        cursor.GetComponent<worldCursor>().selectedOff();

                        canvasDefault.SetActive(true);

                        canvasScale.SetActive(false);
                        canvasRotation.SetActive(false);
                        canvasTransUpDown.SetActive(false);
                        canvasTransLeftRight.SetActive(false);
                        canvasTransForBack.SetActive(false);

                        //errorTextField1.text = "parou de escalar";
                        //fatherObject.parent = originalParent;
                        // fatherObject.localEulerAngles = new Vector3(0f, fatherObject.localEulerAngles.y, 0f);     //preserves the pitch and roll rotations (hololens takes care of them), only changing the yaw

                    }
                    //data.localPosition = new Vector3(data.localPosition.x, data.localPosition.y, data.localPosition.z - transIncrease);


                    break;

                case 3:
                case 4:

                    translatingLeftRight = !translatingLeftRight;

                    if (translatingLeftRight)
                    {
                        scaling = false;
                        rotating = false;
                        translatingUpDown = false;
                        translatingForBack = false;

                        canvasTransLeftRight.SetActive(true);

                        canvasDefault.SetActive(false);
                        canvasScale.SetActive(false);
                        canvasTransUpDown.SetActive(false);
                        canvasTransForBack.SetActive(false);
                        canvasRotation.SetActive(false);

                        gameObject.GetComponent<LineRenderer>().material = scalingMat;

                        lastPosLeftRight = transform.localPosition.x;

                        pointFrozen = hand.localPosition;




                    }
                    else
                    {
                        gameObject.GetComponent<LineRenderer>().material = dataMat;

                        cursor.GetComponent<worldCursor>().selectedOff();

                        canvasDefault.SetActive(true);

                        canvasScale.SetActive(false);
                        canvasRotation.SetActive(false);
                        canvasTransUpDown.SetActive(false);
                        canvasTransLeftRight.SetActive(false);
                        canvasTransForBack.SetActive(false);

                        //errorTextField1.text = "parou de escalar";
                        //fatherObject.parent = originalParent;
                        // fatherObject.localEulerAngles = new Vector3(0f, fatherObject.localEulerAngles.y, 0f);     //preserves the pitch and roll rotations (hololens takes care of them), only changing the yaw

                    }
                    //data.localPosition = new Vector3(data.localPosition.x - transIncrease, data.localPosition.y, data.localPosition.z);


                    break;

                case 5:
                case 6:

                    translatingUpDown = !translatingUpDown;

                    if (translatingUpDown)
                    {
                        scaling = false;
                        rotating = false;
                        translatingLeftRight = false;
                        translatingForBack = false;

                        canvasTransUpDown.SetActive(true);

                        canvasDefault.SetActive(false);
                        canvasScale.SetActive(false);
                        canvasTransLeftRight.SetActive(false);
                        canvasTransForBack.SetActive(false);
                        canvasRotation.SetActive(false);

                        gameObject.GetComponent<LineRenderer>().material = scalingMat;

                        lastPosUpDown = transform.localPosition.y;

                        pointFrozen = hand.localPosition;




                    }
                    else
                    {
                        gameObject.GetComponent<LineRenderer>().material = dataMat;

                        cursor.GetComponent<worldCursor>().selectedOff();

                        canvasDefault.SetActive(true);

                        canvasScale.SetActive(false);
                        canvasRotation.SetActive(false);
                        canvasTransUpDown.SetActive(false);
                        canvasTransLeftRight.SetActive(false);
                        canvasTransForBack.SetActive(false);

                        //errorTextField1.text = "parou de escalar";
                        //fatherObject.parent = originalParent;
                        // fatherObject.localEulerAngles = new Vector3(0f, fatherObject.localEulerAngles.y, 0f);     //preserves the pitch and roll rotations (hololens takes care of them), only changing the yaw

                    }
                    //data.localPosition = new Vector3(data.localPosition.x, data.localPosition.y + transIncrease, data.localPosition.z);


                    break;

            }
        }

        public void rotSelected()
        {


            rotating = !rotating;



            if (rotating)
            {
                scaling = false;
                translatingForBack = false;
                translatingUpDown = false;
                translatingLeftRight = false;

                canvasRotation.SetActive(true);

                canvasDefault.SetActive(false);
                canvasScale.SetActive(false);
                canvasTransUpDown.SetActive(false);
                canvasTransLeftRight.SetActive(false);
                canvasTransForBack.SetActive(false);

                gameObject.GetComponent<LineRenderer>().material = scalingMat;

                lastRotY = transform.localEulerAngles.y;

                pointFrozen = hand.localPosition;




            }
            else
            {
                gameObject.GetComponent<LineRenderer>().material = dataMat;

                cursor.GetComponent<worldCursor>().selectedOff();

                canvasDefault.SetActive(true);

                canvasScale.SetActive(false);
                canvasRotation.SetActive(false);
                canvasTransUpDown.SetActive(false);
                canvasTransLeftRight.SetActive(false);
                canvasTransForBack.SetActive(false);

                //errorTextField1.text = "parou de escalar";
                //fatherObject.parent = originalParent;
                // fatherObject.localEulerAngles = new Vector3(0f, fatherObject.localEulerAngles.y, 0f);     //preserves the pitch and roll rotations (hololens takes care of them), only changing the yaw

            }



        }

        public void scaleSelected()
        {


            scaling = !scaling;



            if (scaling)
            {
                rotating = false;
                translatingForBack = false;
                translatingUpDown = false;
                translatingLeftRight = false;

                canvasScale.SetActive(true);

                canvasDefault.SetActive(false);
                canvasRotation.SetActive(false);
                canvasTransUpDown.SetActive(false);
                canvasTransLeftRight.SetActive(false);
                canvasTransForBack.SetActive(false);

                /* foreach (GameObject child in subtitlesFather)
                 {
                     child.SetActive(false);
                 }*/

                    //subtitlesFather.GetChild(0).gameObject.SetActive(true);

                    gameObject.GetComponent<LineRenderer>().material = scalingMat;

            lastScale = transform.localScale.x;
            //errorTextField1.text = "vai escalar";
            pointFrozen = hand.localPosition;

            // fatherObject.parent = transform;
            // fatherObject.parent = Camera.main.transform;


        }
        else
        {
            gameObject.GetComponent<LineRenderer>().material = dataMat;

            cursor.GetComponent<worldCursor>().selectedOff();

            canvasDefault.SetActive(true);

            canvasScale.SetActive(false);
            canvasRotation.SetActive(false);
            canvasTransUpDown.SetActive(false);
            canvasTransLeftRight.SetActive(false);
            canvasTransForBack.SetActive(false);

            //errorTextField1.text = "parou de escalar";
            //fatherObject.parent = originalParent;
            // fatherObject.localEulerAngles = new Vector3(0f, fatherObject.localEulerAngles.y, 0f);     //preserves the pitch and roll rotations (hololens takes care of them), only changing the yaw

        }



    }

    public Vector3 SphericalToCartesian2D(float polar, float radius)
    {
        Vector3 outCart;

        radius = -(radius + baselineValue) / 20;              //runs through all the data and collect the lowest value as the "baseline", ie the most negative value in the data. I adjusted it like this so the origin is set on it. 
                                                             

       // Debug.Log(radius);

        float a = radius * Mathf.Cos(0);
        outCart.x = a * Mathf.Cos(polar);
        outCart.y = radius * Mathf.Sin(0);
        outCart.z = a * Mathf.Sin(polar);

        return outCart;
    }



}

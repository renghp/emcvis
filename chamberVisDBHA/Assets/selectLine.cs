﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class selectLine : MonoBehaviour {

    public GameObject canvasDB;
    public Text dbTextValue;

    public Transform cursor;

    public Transform dbObject;
    public Transform fieldCenter;
    public Transform child;

    bool selected;

    //GameObject childObj;

    // Use this for initialization
    void Start () {
        selected = false;

        child.gameObject.SetActive(selected);

    }
	
	// Update is called once per frame
	void Update () {

        if(selected)
        {
            canvasDB.SetActive(true);

            float intensity = float.Parse(gameObject.GetComponent<Text>().text);

            float dist = Vector3.Distance(dbObject.position, fieldCenter.position);

            if (dist > 1)
                intensity = intensity / dist;

            dbTextValue.text = intensity.ToString("0.###") + " dB";
        }
		
	}

    void OnSelect()
    {
        selected = !selected;

        child.gameObject.SetActive(selected);

        if (selected)
        {

            child.position = dbObject.position;
            dbObject.GetComponent<followPlaceHolder>().changePlaceHolder(child);

            foreach (Transform child in transform.parent)
            {
                child.gameObject.GetComponent<BoxCollider>().enabled = false;
            }

            gameObject.GetComponent<BoxCollider>().enabled = true;

        }
        else
        {
            dbObject.GetComponent<followPlaceHolder>().changePlaceHolder(cursor);

            foreach (Transform child in transform.parent)
            {
                child.gameObject.GetComponent<BoxCollider>().enabled = true;
            }

            dbTextValue.text = "Last value:\n" + dbTextValue.text;

        }
        

    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class adjustDataScale : MonoBehaviour
{

    public Text debug;

    // public GameObject[] attButtons;


    public Transform data;

    public bool upOrDown;

    private float scaleIncrease;

    public bool freeScaling;


    // Use this for initialization
    void Start()
    {
        scaleIncrease = 0.2f;
    }

    // Update is called once per frame
    void Update()
    {



    }

    void OnSelect()
    {

        if (freeScaling)
        {
            data.GetComponent<postParsingDataController>().scaleSelected();
           // canvasScale.SetActive(true);
        }
        else if (upOrDown)
        {
            //debug.text = "vai aumentar";
            data.localScale = new Vector3(data.localScale.x + scaleIncrease, data.localScale.y, data.localScale.z + scaleIncrease);
            // debug.text = "x scale:\n" + data.localScale.x;

            StartCoroutine(CustomCoroutineAsync());

            gameObject.GetComponent<MeshRenderer>().enabled = false;

        }
        else if (data.localScale.x > scaleIncrease+0.2f)
        {
            //debug.text = "vai diminuir";
            data.localScale = new Vector3(data.localScale.x - scaleIncrease, data.localScale.y, data.localScale.z - scaleIncrease);
            // debug.text = "x scale:\n" + data.localScale.x;

            StartCoroutine(CustomCoroutineAsync());

            gameObject.GetComponent<MeshRenderer>().enabled = false;
        }



        if (!freeScaling)
            data.gameObject.GetComponent<LineRenderer>().startWidth = data.transform.localScale.x / 30f;






    }

    IEnumerator CustomCoroutineAsync()
    {
        yield return new WaitForSeconds(0.1f);

        gameObject.GetComponent<MeshRenderer>().enabled = true;
    }

    /*public void cursorHit()
    {
        Debug.Log("colidiu");
        gameObject.GetComponent<Renderer>().material = highlightedMat;
    }

    public void unHit()
    {
        Debug.Log("descolidiu");
        gameObject.GetComponent<Renderer>().material = defaultMat;
    }*/

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("colidiu");

        if (other.gameObject.tag == "selectorCube")
        {
            OnSelect();
        }
    }

    private void OnTriggerExit(Collider other)
    {

        //Debug.Log("descolidiu");

        if (other.gameObject.tag == "selectorCube")
        {
            Debug.Log("Cubo Saiu");
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class adjustDataTranslation : MonoBehaviour
{

    public Text debug;

    // public GameObject[] attButtons;

    public Transform data;

    public int direction;   //1,2 - forward/backward; 3/4 - right/left; 5/6 - up/down

    private float transIncrease;

    public bool freeTranslating;

    // Use this for initialization
    void Start()
    {
        transIncrease = 0.1f;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnSelect()
    {
        //attButtons = GameObject.FindGameObjectsWithTag("attButton");

        //string attribute;
        // attribute = gameObject.name;

        if (freeTranslating)
        {
            data.GetComponent<postParsingDataController>().translationSelected(direction);
        }

        else
        {
            switch (direction)
            {
                case 1:
                    data.localPosition = new Vector3(data.localPosition.x, data.localPosition.y, data.localPosition.z - transIncrease);
                    break;
                case 2:
                    data.localPosition = new Vector3(data.localPosition.x, data.localPosition.y, data.localPosition.z + transIncrease);
                    break;
                case 3:
                    data.localPosition = new Vector3(data.localPosition.x - transIncrease, data.localPosition.y, data.localPosition.z);
                    break;
                case 4:
                    data.localPosition = new Vector3(data.localPosition.x + transIncrease, data.localPosition.y, data.localPosition.z);
                    break;
                case 5:
                    data.localPosition = new Vector3(data.localPosition.x, data.localPosition.y + transIncrease, data.localPosition.z);
                    break;
                case 6:
                    data.localPosition = new Vector3(data.localPosition.x, data.localPosition.y - transIncrease, data.localPosition.z);
                    break;

            }

            StartCoroutine(CustomCoroutineAsync());

            gameObject.GetComponent<MeshRenderer>().enabled = false;
        }






        


    }

    IEnumerator CustomCoroutineAsync()
    {
        yield return new WaitForSeconds(0.1f);

        gameObject.GetComponent<MeshRenderer>().enabled = true;
    }



}

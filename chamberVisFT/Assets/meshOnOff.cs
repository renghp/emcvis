﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloLensHandTracking;

public class meshOnOff : MonoBehaviour {

    public Transform placeHolder;

    private bool meshStatus;

    public GameObject roomMesh;

    public Material defaultMat, selectedMat, highlightedSelected, highlightedDefault;

	// Use this for initialization
	void Start () {

        meshStatus = false;
        OnSelect();

    }
	
	// Update is called once per frame
	void Update () {

        transform.position = placeHolder.position;
        transform.eulerAngles = placeHolder.eulerAngles;

    }

    void OnSelect()
    {

        Debug.Log("entrou");
        meshStatus = !meshStatus;



        if (meshStatus)
        {
            roomMesh.SetActive(false);
            gameObject.GetComponent<MeshRenderer>().material = defaultMat;

            if (gameObject.tag == "attButton")
            {
                gameObject.GetComponent<changeAttribute>().defaultMat = defaultMat;
                gameObject.GetComponent<changeAttribute>().highlightedMat = highlightedSelected;

            }

        }
        else
        {

            roomMesh.SetActive(true);
            gameObject.GetComponent<MeshRenderer>().material = selectedMat;

            if (gameObject.tag == "attButton")
            {
                gameObject.GetComponent<changeAttribute>().defaultMat = selectedMat;
                gameObject.GetComponent<changeAttribute>().highlightedMat = highlightedDefault;
            }

        }



    }
}



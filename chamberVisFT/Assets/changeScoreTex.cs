﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeScoreTex : MonoBehaviour {

    public Material[] scores;


    // Use this for initialization
    void Start()
    {
        
    }

    void initializeClass()
    {
        scores = new Material[11];

        int i = 0;
        GameObject gb = GameObject.FindWithTag("wayPoint");
        foreach (Transform child in gb.transform)
        {
            scores[i] = child.GetComponent<Renderer>().material;

            //Debug.Log(scores[i].name);
            i++;
        }
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void changeScore(int perc)
    {
        initializeClass();

        int index = (int)Math.Floor((float)perc / 10);

        //Debug.Log(index);

        //Debug.Log(scores[index].name);

        gameObject.GetComponent<Renderer>().material = scores[index];
    }
}

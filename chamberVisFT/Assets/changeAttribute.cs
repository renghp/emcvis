﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class changeAttribute : MonoBehaviour
{
    //public GameObject seatScoreController;

    public Material defaultMat, highlightedMat;

    public Text debug;

    public GameObject[] attButtons;

    public bool holdable;

    public bool on = false;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnSelect()
    {
        //attButtons = GameObject.FindGameObjectsWithTag("attButton");

        // string attribute;
        // = gameObject.name;

        on = true;

        if (!holdable)
        {
            foreach (GameObject aB in attButtons)
                aB.GetComponent<MeshRenderer>().enabled = true;


            gameObject.GetComponent<MeshRenderer>().enabled = false;
        }
        

       // seatScoreController.GetComponent<seatScores67>().changeAttribute(transform.GetSiblingIndex());

    }

    public void selectedOff()
    {
        on = false;
        gameObject.GetComponent<MeshRenderer>().enabled = true;
    }

    public void cursorHit()
    {
        Debug.Log("colidiu");
        gameObject.GetComponent<Renderer>().material = highlightedMat;
    }

    public void unHit()
    {
        Debug.Log("descolidiu");
        gameObject.GetComponent<Renderer>().material = defaultMat;
    }


    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("colidiu");

        if (other.gameObject.tag == "selectorCube")
        {
            if (on)
            {
                selectedOff();
            }
            else
            {
                OnSelect();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {

        //Debug.Log("descolidiu");

        if (other.gameObject.tag == "selectorCube")
        {
            Debug.Log("Cubo Saiu");
        }
    }

}
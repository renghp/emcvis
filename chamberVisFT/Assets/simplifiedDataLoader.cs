﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using System;
using System.IO;
using System.Text;
using UnityEngine.UI;

#if !UNITY_EDITOR
    using System.Threading.Tasks;
#endif

public class simplifiedDataLoader : MonoBehaviour
{

    public Text errorTextField1;

    public Transform prefab;

    public String parsedCartesianFilePath;

    public GameObject arrows;

    // Use this for initialization
    void Start()
    {

#if UNITY_EDITOR
        cartesianFileParser(parsedCartesianFilePath);
#endif

#if !UNITY_EDITOR

            
            asyncWrapper();

            

            
#endif



        


    }

    public void changeFreq(string newParsedCartesianFilePath)
    {

        parsedCartesianFilePath = newParsedCartesianFilePath;

        gameObject.GetComponent<LineRenderer>().positionCount = 0;

        Start();

    }

    // Update is called once per frame
    void Update()
    {

    }

#if !UNITY_EDITOR
    private async Task asyncWrapper()
    {
        await cartesianFileParserAsync(parsedCartesianFilePath);
    }
#endif

#if !UNITY_EDITOR

    private async Task cartesianFileParserAsync(string parsedCartesianFile)
    {
        arrows.SetActive(false);


        TextAsset dataAsset = Resources.Load(parsedCartesianFile) as TextAsset;     //filename without the frigging extension



        string patternValues = @"(.*)\t(.*)\t(.*)";         //regex to get all values

        //


        Regex regexValues = new Regex(patternValues, RegexOptions.IgnoreCase);

        Match matchesValues = regexValues.Match(dataAsset.text);
        int matchCount = 1;

        gameObject.GetComponent<LineRenderer>().positionCount = 0;      //erases the last lineRenderer;

        gameObject.GetComponent<LineRenderer>().enabled = false;

        while (matchesValues.Success)
        {
            // Debug.Log("matched " + matchesValues.Groups[1].Value);

            Transform a;

            // double freq = Double.Parse(matchesValues.Groups[1].Value);

            Vector3 pos = new Vector3(float.Parse(matchesValues.Groups[1].Value), float.Parse(matchesValues.Groups[2].Value), float.Parse(matchesValues.Groups[3].Value));


            a = Instantiate(prefab, pos, Quaternion.identity, transform);
            a.localPosition = a.position;

            gameObject.GetComponent<LineRenderer>().positionCount = matchCount;
            gameObject.GetComponent<LineRenderer>().SetPosition(matchCount - 1, a.localPosition);


            Destroy(a.gameObject);

            matchesValues = matchesValues.NextMatch();
            matchCount++;

            if (matchCount%50 == 0)
            {
                await Task.Delay(1);
              //  errorTextField2.text = "n " + matchCount;

                //errorTextField1.text = "Loading: \n" + matchCount + "%";
            
                errorTextField1.text = "Loading: \n" + (int)(matchCount*100 / 2000) + "%";      //2000 is the maximum number of points based on the simplification
            }


            // errorTextField1.text = "n " + matchCount;
        }

        //Debug.Log("done with matching");

        gameObject.GetComponent<LineRenderer>().startWidth = transform.localScale.x / 600f;

       

        prefab.gameObject.SetActive(false);

        BoxCollider bc = gameObject.AddComponent<BoxCollider>();

        errorTextField1.text = "Perform a quick tap gesture while\naiming at one of the buttons in your\nhand menu to select it";

        gameObject.GetComponent<LineRenderer>().enabled = true;
        
        arrows.SetActive(true);
        

    }

#endif

    void cartesianFileParser(string parsedCartesianFile)
    {

        arrows.SetActive(false);

        TextAsset dataAsset = Resources.Load(parsedCartesianFile) as TextAsset;     //filename without the frigging extension



        string patternValues = @"(.*)\t(.*)\t(.*)";         //regex to get all values

        //

        Regex regexValues = new Regex(patternValues, RegexOptions.IgnoreCase);

        Match matchesValues = regexValues.Match(dataAsset.text);
        int matchCount = 1;

        gameObject.GetComponent<LineRenderer>().positionCount = 0;      //erases the last lineRenderer;

        while (matchesValues.Success)
        {
            // Debug.Log("matched " + matchesValues.Groups[1].Value);

            Transform a;

            // double freq = Double.Parse(matchesValues.Groups[1].Value);

            Vector3 pos = new Vector3(float.Parse(matchesValues.Groups[1].Value), float.Parse(matchesValues.Groups[2].Value), float.Parse(matchesValues.Groups[3].Value));


            a = Instantiate(prefab, pos, Quaternion.identity, transform);
            a.localPosition = a.position;

            gameObject.GetComponent<LineRenderer>().positionCount = matchCount;
            gameObject.GetComponent<LineRenderer>().SetPosition(matchCount - 1, a.localPosition);


            Destroy(a.gameObject);

            matchesValues = matchesValues.NextMatch();
            matchCount++;


            // errorTextField1.text = "n " + matchCount;
        }

        //Debug.Log("done with matching");

        gameObject.GetComponent<LineRenderer>().startWidth = transform.localScale.x / 600f;

       

        prefab.gameObject.SetActive(false);
        arrows.SetActive(true);

         BoxCollider bc = gameObject.AddComponent<BoxCollider>();


    }
}

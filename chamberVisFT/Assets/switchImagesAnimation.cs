﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class switchImagesAnimation : MonoBehaviour {


    public Sprite [] img;
    private int iterator = 0;





    // Use this for initialization
    void Start () {

        InvokeRepeating("changeImage", 0.0f, 0.5f);

        gameObject.GetComponent<Image>().sprite = img[0];

    }
	
	// Update is called once per frame
	void Update () {

      

    }

    void changeImage()
    {
        if (iterator < img.Length-1)
        {
            iterator++;
            //Debug.Log(iterator);
            gameObject.GetComponent<Image>().sprite = img[iterator];

        }
        else
            iterator = -1;
    }
}
